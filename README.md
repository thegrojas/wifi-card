

## References

- [Designing Print-Ready Business Cards in Inkscape](https://logosbynick.com/designing-print-ready-business-cards-in-inkscape/)
- Icons made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
